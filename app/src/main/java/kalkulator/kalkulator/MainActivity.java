package kalkulator.kalkulator;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button n1, n2, n3, n4, n5, n6, n7, n8, n9 ,n0, plus, minus, puta, podjeljeno, rjejenje, tocka, brisi;

    TextView upis, upisano, operacijone;

    Button[] numArray, opArray;

    double rjesenje, brojDrugi, brojPrvi;

    String operacija;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        n0 = (Button)findViewById(R.id.N0);
        n1 = (Button)findViewById(R.id.N1);
        n2 = (Button)findViewById(R.id.N2);
        n3 = (Button)findViewById(R.id.N3);
        n4 = (Button)findViewById(R.id.N4);
        n5 = (Button)findViewById(R.id.N5);
        n6 = (Button)findViewById(R.id.N6);
        n7 = (Button)findViewById(R.id.N7);
        n8 = (Button)findViewById(R.id.N8);
        n9 = (Button)findViewById(R.id.N9);
        upis = (TextView)findViewById(R.id.upis);
        plus =(Button)findViewById(R.id.plus);
        minus =(Button)findViewById(R.id.minus);
        puta =(Button)findViewById(R.id.puta);
        podjeljeno =(Button)findViewById(R.id.podjeljeno);
        tocka = (Button)findViewById(R.id.tocka);
        rjejenje = (Button)findViewById(R.id.Rjesenje);
        brisi = (Button)findViewById(R.id.brisanje);
        upisano = (TextView)findViewById(R.id.upisano);
        operacijone = (TextView)findViewById(R.id.operacija);

        numArray = new Button[]{n1, n2, n3, n4, n5, n6, n7, n8, n9, n0, tocka};
        opArray = new Button[]{plus, minus, puta, podjeljeno};

        upis.setBackgroundColor(Color.BLACK);
        upis.setTextColor(Color.WHITE);
        upisano.setBackgroundColor(Color.BLACK);
        upisano.setTextColor(Color.WHITE);
        operacijone.setBackgroundColor(Color.BLACK);
        operacijone.setTextColor(Color.WHITE);
        upis.setTextSize(25);
        upisano.setTextSize(25);
        operacijone.setTextSize(25);

        for(Button b:numArray){
            b.setOnClickListener(this);
        }

        brisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder sb = new StringBuilder();
                String s = upis.getText().toString();
                for(int i = 0; i < upis.length()-1; i++){
                    sb.append(s.charAt(i));
                }
                upis.setText(sb);
            }
        });

        for(Button b:opArray){
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button b = (Button) v;
                    brojPrvi = Double.parseDouble(upis.getText().toString());
                    switch(b.getText().toString()){
                        case "+": operacija = "+";
                            operacijone.setText(b.getText());
                            break;
                        case "-": operacija = "-";
                            operacijone.setText(b.getText());
                            break;
                        case "*": operacija = "*";
                            operacijone.setText(b.getText());
                            break;
                        case "/": operacija = "/";
                            operacijone.setText(b.getText());
                            break;
                    }
                    upis.setText("");
                    upisano.setText(Double.toString(brojPrvi));
                }

            });
        }



        rjejenje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brojDrugi = Double.parseDouble(upis.getText().toString());
                switch(operacija){
                    case "+": rjesenje =  brojPrvi + brojDrugi;
                        break;
                    case "-": rjesenje =  brojPrvi - brojDrugi;
                        break;
                    case "*": rjesenje =  brojPrvi * brojDrugi;
                        break;
                    case "/": rjesenje =  brojPrvi / brojDrugi;
                        break;
                }
                operacijone.setText("");
                String stringdouble= Double.toString(rjesenje);
                upis.setText(stringdouble);
                upisano.setText("");
            }

        });

    }

    @Override
    public void onClick(View v) {

        Button b = (Button) v;

        upis.append(b.getText());

    }



}
